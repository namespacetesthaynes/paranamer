package com.gitlab.haynes.paranamer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.gitlab.haynes.paranamer.DefaultParanamer;

import org.junit.jupiter.api.Test;

public class PrimitivesTests {

    @Test
    public void primitivesInArraysShouldBeRetrievable() throws NoSuchMethodException {
        assertEquals(DefaultParanamer.getParameterTypeNamesCSV(new Class[]{String.class}), "java.lang.String");
        assertEquals(DefaultParanamer.getParameterTypeNamesCSV(new Class[]{String.class, String.class}), "java.lang.String,java.lang.String");
        assertEquals(DefaultParanamer.getParameterTypeNamesCSV(new Class[]{String[].class, Object[].class}), "java.lang.String[],java.lang.Object[]");
        assertEquals(DefaultParanamer.getParameterTypeNamesCSV(new Class[]{String[][].class, Object[][].class}), "java.lang.String[][],java.lang.Object[][]");
        assertEquals(DefaultParanamer.getParameterTypeNamesCSV(new Class[]{long[].class}), "long[]");
        assertEquals(DefaultParanamer.getParameterTypeNamesCSV(new Class[]{long[][].class}), "long[][]");
        assertEquals(DefaultParanamer.getParameterTypeNamesCSV(new Class[]{int[].class}), "int[]");
        assertEquals(DefaultParanamer.getParameterTypeNamesCSV(new Class[]{int[][].class}), "int[][]");
        assertEquals(DefaultParanamer.getParameterTypeNamesCSV(new Class[]{boolean[].class}), "boolean[]");
        assertEquals(DefaultParanamer.getParameterTypeNamesCSV(new Class[]{boolean[][].class}), "boolean[][]");
        assertEquals(DefaultParanamer.getParameterTypeNamesCSV(new Class[]{short[].class}), "short[]");
        assertEquals(DefaultParanamer.getParameterTypeNamesCSV(new Class[]{short[][].class}), "short[][]");
        assertEquals(DefaultParanamer.getParameterTypeNamesCSV(new Class[]{float[].class}), "float[]");
        assertEquals(DefaultParanamer.getParameterTypeNamesCSV(new Class[]{float[][].class}), "float[][]");
        assertEquals(DefaultParanamer.getParameterTypeNamesCSV(new Class[]{double[].class}), "double[]");
        assertEquals(DefaultParanamer.getParameterTypeNamesCSV(new Class[]{double[][].class}), "double[][]");
        assertEquals(DefaultParanamer.getParameterTypeNamesCSV(new Class[]{byte[].class}), "byte[]");
        assertEquals(DefaultParanamer.getParameterTypeNamesCSV(new Class[]{byte[][].class}), "byte[][]");
        assertEquals(DefaultParanamer.getParameterTypeNamesCSV(new Class[]{char[].class}), "char[]");
        assertEquals(DefaultParanamer.getParameterTypeNamesCSV(new Class[]{char[][].class}), "char[][]");
    }
}
