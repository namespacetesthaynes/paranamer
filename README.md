# ![Paranamer](http://paulhammant.com/images/ParaNamer.jpg)

## Method Parameter Name Access for Java 11

# What is it?

This is a fork of https://github.com/paul-hammant/paranamer


It is a library that allows the parameter names of non-private methods and constructors to be accessed at runtime. Normally this information is dropped by the compiler. In effect, methods like `doSometing(mypkg.Person toMe)`
currently look like `doSomething(mypackage.Person ???)` to people using Java's reflection to inspect methods.

To date parameter name access has not been very useful to Java application developers, but with the advent of advanced scripting languages and web action frameworks for the JVM it is of increasing importance to be able to leverage a method's parameter names. Scripting languages like [Groovy](http://groovy.codehaus.org/) and [JRuby](http://jruby.codehaus.org/), web action frameworks like [Waffle](http://waffle.codehaus.org) and [VRaptor](# "http://www.vraptor.org/") (that verge on the transparent) and the compelling [Grails](http://grails.codehaus.org/). SOAP and REST designs could also benefit.

*Paranamer* allows you to generate and use parameter name info for versions of Java 11 and above. Historically, it was felt that applications could end up depending on parameter names, and that they essentially became part of constructor/method signatures and could never be changed if you wanted to be backwards compatible.  The view of the authors of *Paranamer* is that you should be aware that parameter names may change between releases, and code to not depend on them.

*Paranamer* is Open Source, and licensed as BSD, and first created in in July 2006. It is compatible with commercial/proprietary, GPL, BSD, and Apache (or any open/free source) use.

# Java8 has parameter name access built in!

JDK 8 though has native support builtin, and [stackoverflow](http://stackoverflow.com/questions/21455403/how-to-get-method-parameter-names-in-java-8-using-reflection) shows you how to make that work.
Unfortunately this is still hidden behind a feature flag that every project who wants to use it need to enable manually.

# Accessing Parameter Name data

There is a method called `lookupParameterNames` that returns an array of strings for a method or constructor.

```java
// MySomethingOrOther.java**
Method method = Foo.class.getMethod(...);

Paranamer paranamer = new CachingParanamer();

String[] parameterNames = paranamer.lookupParameterNames(method) // throws ParameterNamesNotFoundException if not found

// or:

parameterNames = paranamer.lookupParameterNames(method, false) // will return null if not found
```

*Paranamer* does not have any runtime jar dependencies while looking up parameter info previously generated that's been zipped into a jar.

## DefaultParanamer

DefaultParanamer tries to read parameter name data from an extra public static field called `__PARANAMER_DATA` on the class. This field need to be added after compilation of the class, and before you put the resulting classes in a jar.

The static field essentially looks like the following. You really do not need to know this unless your going to make something compatible with *Paranamer*:

```java
public static final String __PARANAMER_DATA = "v1.0 \n"
      + "<init> com.example.PeopleService peopleService \n"
      + "setName java.lang.String,java.lang.String givenName,familyName \n";
      + "setDateOfBirth int,int,int day,month,year \n";
```

Clearly the method's source needs to be analysed and lines added per method to that `__PARANAMER_DATA` field. See below.

## BytecodeReadingParanamer

If generating meta data for parameter names at compile time is not for you, try class `BytecodeReadingParanamer` as a runtime only solution. This uses a cut down forked and cut-down version of ASM to extract debug information from a class at runtime. As it happens this is the fallback implementation for `CachingParanamer` when `DefaultParanamer` reports that there is no meta data for a class.

Note: BytecodeReadingParanamer does not work parameters stored in **interfaces**, because the javac compiler ALWAYS omits that information from the debug tables in the .class file.

## JavadocParanamer

Pulls its parameter names from a Javadoc zip/jar (named in the constructor). Courtesy of Sam Halliday

## PositionalParanamer

Numbers it's parameter names arg0, arg1, arg2 (etc), intended as a fall-back if you need it. From Stefan Fleiter.

## AnnotationParanamer

`AnnotationParanamer` uses the `@Named` annotation from JSR 330 and extracts names pertinent to parameters from that.

```java
public static class Something {
    public void doSomething(@Named("usedName") String ignoredName) {
    }
}
```

`AnnotationParanamer` takes a delegate paranamer instance as an optional constructor arg.  This will allow constructors and methods to only partly leverage `@Named`, with other parameters having non-annotated parameter names (the via say `DefaultParanamer` or `BytecodeReadingParanamer`).

If you have an alternate annotation to `@Named`, then you can specify that in a subclass of `AnnotationParanamer` that overrides two methods isNamed and getNamedValue.  Your overridden methods should do the equivalent of 'return an instance of Named' and `return ((Named) ann).value();` respectively.

If you are using `@Named` annotation, you will need to add `javax.atinject` artifact in your classpath. *Paranamer* is built with the `javax.atinject` module as an optional dependency.

## AdaptiveParanamer

`AdaptiveParanamer` is designed for using a series of *Paranamer* implementations together. The first supplied is asked if it can supply parameter name data for a constructor/method.  If it cannot, then the next one is asked and so on.  The default constructor for this uses `DefaultParanamer` with `ByteCodeReadingParanamer` as its contingency.

## CachingParanamer

`CachingParanamer` stores the results of each parameter name lookup, so that second and subsequent invocations will be far quicker.

There's a subclass of `CachingParanamer` called `CachingParanamer.WithoutWeakReferences`. It does not use a WeakHashMap as an internal implementation. If you're great with profiling of applications under load, you might be able to justify use of this implementation for your particular app.


# Feeding DefaultParanamer

##  Generating __PARANAMER_DATA with Maven 2 or 3

For Maven, configuration is simpler. Just add this to the `build/plugins` section of your `pom.xml`:

```xml
<plugin>
    <groupId>com.gitlab.haynes.paranamer</groupId>
    <artifactId>paranamer-maven-plugin</artifactId>
    <executions>
        <execution>
            <id>run</id>  <!-- id is optional -->
            <configuration>
                <sourceDirectory>${project.build.sourceDirectory}</sourceDirectory>
                <outputDirectory>${project.build.outputDirectory}</outputDirectory>
            </configuration>        
             <goals>
                <goal>generate</goal>
            </goals>
        </execution>
    </executions>
    <dependencies>
        <!-- if some of parameter names you need to retain are held in pre-existing jars, they need to be added to the classpath -->
        <dependency>
            <groupId>some-artifact-group</groupId>
            <artifactId>some-artifact</artifactId>
            <version>1.0</version>
        </dependency>
    </dependencies>
</plugin>
```

The classes in the ultimate jar file will automatically be made with parameter name data.

## Embedding Paranamer in your jar

There are already too many jar's for day to day Java development right? Simply consume the runtime *Paranamer* jar into your project's jar using the Maven2 'shade' plugin.

```xml
<!-- Put in your POM.xml file -->
<build>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-shade-plugin</artifactId>
        <executions>
          <execution>
            <phase>package</phase>
            <goals>
              <goal>shade</goal>
            </goals>
            <configuration>
              <shadedArtifactId>artifact-shaded</shadedArtifactId>
              <relocations>
                <relocation>
                  <pattern>com.gitlab.haynes.paranamer</pattern>
                  <shadedPattern>com.yourcompany.yourapp.paranamer</shadedPattern>
                </relocation>
              </relocations>
            </configuration>
          </execution>
        </executions>
      </plugin>
    </plugins>
  </build>
```

# What if a parameter names changes?

The general answer to this question is that you should not ship something to third parties where they are going to hard-core your parameter names into their application. For you own in-house stuff, accessing parameter names is harmless. You should be able to ripple though the source code of even large applications, and change say `badSpeeldWord` to `badlySpelledWorld` if you need to.

# Other Modules

* paranamer-maven-plugin - a Maven plugin (as shown above)

# Paranamer's Future

The need for *Paranamer* will go away when [JEP-118](http://openjdk.java.net/jeps/118) lands as part of Java8.  Despite that, we intend to maintain the library so that it continues to work.

# Project Information

## Source Code

[See Project On GitLab](https://gitlab.com/haynes/paranamer) (yes merge requests are fine).

## Committers & Contributors

* Paul Hammant
* Mauro Talevi
* Guilherme Silveira
* Sam Halliday
* Stefan Fleiter
* Robert Scholte
* Raghuram Devarakonda
* Nicholas Whitehead
* Otávio Garcia
* Hannes Rosenögger

## Other Codehaus Project Info

GitLab's [issue tracker for Paranamer](https://gitlab.com/haynes/paranamer/issues) is kinda how conversations happen re Paranamer.

## Downloads

Download the latest released jar files (2.9.0) [here](http://central.maven.org/maven2/com/gitlab/haynes/paranamer/paranamer/2.9.0/)

# More Examples

The unit tests for Paranamer illustrate some more way to use it: [https://gitlab.com/haynes/paranamer/tree/master/paranamer/src/test/com/gitlab/haynes/paranamer](https://gitlab.com/haynes/paranamer/tree/master/paranamer/src/test/com/gitlab/haynes/paranamer)
